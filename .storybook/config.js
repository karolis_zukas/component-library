import { configure, addDecorator } from '@storybook/react';
const { withKnobs } = require('@storybook/addon-knobs/react');

addDecorator(withKnobs);

const req = require.context('../packages', true, /.stories.js$/);

const loadStories = () => {
    req.keys().forEach((filename) => req(filename))
};

configure(loadStories, module);
