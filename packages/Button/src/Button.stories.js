import React from "react";
import { storiesOf } from "@storybook/react";
import { text } from '@storybook/addon-knobs/react';
import Button from './Button';

storiesOf("Button", module).add("Default", () => (
    <Button onClick={() => { alert('clicky clicky')}}>{text('label', 'Click Me')}</Button>
));