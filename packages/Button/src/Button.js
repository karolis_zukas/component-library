import React from 'react';
import PropTypes from 'prop-types';
import './button.scss';

const Button = ({children, onClick}) => {
    return (
        <button
            className="button"
            type="button"
            onClick={onClick}>
                {children}
        </button>
    );
};

Button.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.node
};

export default Button;