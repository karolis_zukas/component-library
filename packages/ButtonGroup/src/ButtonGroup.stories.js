
import React from "react";
import { storiesOf } from "@storybook/react";
import ButtonGroup from 'button-group';

const items = [
    {id: 1, name: 'first button', onClick: () => { alert('first one') }},
    {id: 2, name: 'second one', onClick: () => { alert('second one') }},
    {id: 3, name: 'third one', onClick: () => { alert('thisr one') }}
];

storiesOf("ButtonGroup", module).add("Default", () => (
    <ButtonGroup items={items}/>
));