### Component library ###

React monorepo, for a UI library.


## Building all components ##
```lerna exec yarn build```

## Running Storybook ##
```start-storybook -p 9001 -c .storybook```

## Publish ##
Build components by running ```yarn build:components``` command;
Publish components by running ```yarn publish:components``` command.