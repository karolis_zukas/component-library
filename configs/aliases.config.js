const path = require('path');

const aliases = {
    'button': path.resolve(__dirname, '../packages/Button/src/index'),
    'button-group': path.resolve(__dirname, '../packages/ButtonGroup/src/index'),
};

module.exports = { aliases };